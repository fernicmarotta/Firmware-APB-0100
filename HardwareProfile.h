/*********************************************************************
 *
 *	Hardware specific definitions for:
 *    - Internet Radio
 *    - PIC18F67J60
 *    - Internal 10BaseT Ethernet
 *
 *********************************************************************
 * FileName:        HardwareProfile.h
 * Dependencies:    Compiler.h
 * Processor:       PIC18
 * Compiler:        Microchip C18 v3.36 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2010 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *		ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *		used in conjunction with a Microchip ethernet controller for
 *		the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date		Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder		09/16/2010	Regenerated for specific boards
 ********************************************************************/
#ifndef HARDWARE_PROFILE_H
#define HARDWARE_PROFILE_H

#include "Compiler.h"

// Define a macro describing this hardware set up (used in other files)
#define INTERNET_RADIO

// Set configuration fuses (but only in MainDemo.c where THIS_IS_STACK_APPLICATION is defined)
#if defined(THIS_IS_STACK_APPLICATION)
	#pragma config WDT=OFF, FOSC2=ON, FOSC=HSPLL, ETHLED=ON

	// Automatically set Extended Instruction Set fuse based on compiler setting
	#if defined(__EXTENDED18__)
		#pragma config XINST=ON
	#else
		#pragma config XINST=OFF
	#endif
#endif


// Clock frequency values
// These directly influence timed events using the Tick module.  They also are used for UART and SPI baud rate generation.
#define GetSystemClock()		(41666667ul)			// Hz
#define GetInstructionClock()	(GetSystemClock()/4)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Might need changing if using Doze modes.
#define GetPeripheralClock()	(GetSystemClock()/4)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Divisor may be different if using a PIC32 since it's configurable.

//Configuramos todos los pines como salida:

#define MP3_XRESET_IO		(LATDbits.LATD0)
#define MP3_XDCS_TRIS		(TRISBbits.TRISB1)	// Data Chip Select
#define MP3_XDCS_IO			(LATBbits.LATB1)
#define MP3_XCS_TRIS		(TRISBbits.TRISB2)	// Control Chip Select
#define MP3_XCS_IO			(LATBbits.LATB2)
#define MP3_SCK_TRIS		(TRISCbits.TRISC3)
#define MP3_SDI_TRIS		(TRISCbits.TRISC4)
#define MP3_SDO_TRIS		(TRISCbits.TRISC5)


// Hardware I/O pin mappings
//#warning "Configurar correctamente el hardware de cada sem�foro"

#if SEMAFORO_ID == 1
//#define DOBLE_AUDIO	
//#define CHICHARRA
//#define MOVILYDOTACION
#elif  SEMAFORO_ID == 2
#define DOBLE_AUDIO
//#define CHICHARRA
//#define MOVILYDOTACION
#elif  SEMAFORO_ID == 3
//#define DOBLE_AUDIO
//#define CHICHARRA
//#define MOVILYDOTACION
#elif  SEMAFORO_ID == 4
#define DOBLE_AUDIO
//#define CHICHARRA
//#define MOVILYDOTACION
#elif  SEMAFORO_ID == 5
//#define DOBLE_AUDIO
//#define CHICHARRA
#define LUZPASILLO
//#define MOVILYDOTACION
#elif  SEMAFORO_ID == 6
#define DOBLE_AUDIO	
#define CHICHARRA
#define TRAMA_API
#define SEMAFORO_BOX1
//#define MOVILYDOTACION
//#define HABILITAR_ENTRADAS
#elif  SEMAFORO_ID == 7
//#define DOBLE_AUDIO
//#define CHICHARRA
//#define MOVILYDOTACION
#elif  SEMAFORO_ID == 8
//#define DOBLE_AUDIO
//#define CHICHARRA
//#define MOVILYDOTACION
#elif  SEMAFORO_ID == 9
//#define DOBLE_AUDIO	
//#define CHICHARRA
//#define MOVILYDOTACION
#elif  SEMAFORO_ID == 10
//#define DOBLE_AUDIO	
//#define CHICHARRA
//#define MOVILYDOTACION
#elif  SEMAFORO_ID == 11
//#define DOBLE_AUDIO	
//#define CHICHARRA
//#define MOVILYDOTACION
#elif  SEMAFORO_ID == 12
//#define DOBLE_AUDIO	
//#define CHICHARRA
//#define MOVILYDOTACION
#elif  SEMAFORO_ID == 13
//#define DOBLE_AUDIO	
//#define CHICHARRA
//#define MOVILYDOTACION
#endif

#define STAND_BY_TRIS (TRISJbits.TRISJ7)
#define STAND_BY_O (LATJbits.LATJ7)
#define WARNING_POTENCIA_TRIS (TRISAbits.TRISA5)
#define WARNING_POTENCIA_I (PORTAbits.RA5)

#ifdef DOBLE_AUDIO
#define STAND_BY_TRIS2 (TRISHbits.TRISH6)
#define STAND_BY_2_O (LATHbits.LATH6)
#define WARNING_POTENCIA_TRIS2 (TRISHbits.TRISH5)
#define WARNING_POTENCIA_2_I (PORTHbits.RH5)
#endif

#ifdef CHICHARRA
#define CHICHARRA_TRIS (TRISEbits.TRISE0)
#define CHICHARRA_O   (LATEbits.LATE0)
#define CHICHARRA_MIRROR_TRIS (TRISFbits.TRISF0)
#define CHICHARRA_MIRROR_O (LATFbits.LATF0)
#endif

#ifdef SEMAFORO_BOX1
#define BOX_ROJO_TRIS (TRISHbits.TRISH0)
#define BOX_ROJO_O    (LATHbits.LATH0)
#define BOX_VERDE_TRIS (TRISHbits.TRISH1)
#define BOX_VERDE_O    (LATHbits.LATH1)
#define BOX_AMARILLO_TRIS (TRISHbits.TRISH2)
#define BOX_AMARILLO_O    (LATHbits.LATH2)
#define BOX_AZUL_TRIS (TRISHbits.TRISH3)
#define BOX_AZUL_O    (LATHbits.LATH3)
#define BOX_BLANCO_TRIS (TRISEbits.TRISE1)
#define BOX_BLANCO_O    (LATEbits.LATE1)    
#endif

#ifdef MOVILYDOTACION
#define BCD3_TRIS (TRISBbits.TRISB4)
#define BCD3_O   (LATBbits.LATB4)
#define BCD2_TRIS (TRISBbits.TRISB5)
#define BCD2_O   (LATBbits.LATB5)
#define BCD1_TRIS (TRISJbits.TRISJ2)
#define BCD1_O   (LATJbits.LATJ2)
#define BCD0_TRIS (TRISJbits.TRISJ3)
#define BCD0_O   (LATJbits.LATJ3)
#define MUXIN_TRIS (TRISGbits.TRISG1)
#define MUXIN_O   (LATGbits.LATG1)
#endif

// Momentary push buttons
#ifdef TRAMA_API
#define SIRENA_MAYOR_TRIS (TRISGbits.TRISG5)
#define SIRENA_MAYOR_O   (LATGbits.LATG5)
#define SEMAF_COLON_TRIS (TRISGbits.TRISG6)
#define SEMAF_COLON_O   (LATGbits.LATG6)
#endif

#ifdef HABILITAR_ENTRADAS
#ifdef LUZPASILLO
#error "Conflicto de pines en P4"
#endif
#ifndef LUZPASILLO
#define INPUT1_TRIS (TRISEbits.TRISE7)
#define INPUT1_I   (PORTEbits.RE7)
#endif
#endif

#ifdef LUZPASILLO
#ifdef HABILITAR_ENTRADAS
#error "Conflicto de pines en P4"        
#endif
#ifndef HABILITAR_ENTRADAS
#define LUZPASILLO1_TRIS (TRISEbits.TRISE6)
#define LUZPASILLO1_O   (LATEbits.LATE6)
#define LUZPASILLO2_TRIS (TRISEbits.TRISE7)
#define LUZPASILLO2_O   (LATEbits.LATE7)
#endif
#endif

// Serial SRAM
#define SPIRAM_CS_TRIS		(TRISEbits.TRISE4)
#define SPIRAM_CS_IO		(LATEbits.LATE4)
#define SPIRAM_SCK_TRIS		(TRISCbits.TRISC3)
#define SPIRAM_SDI_TRIS		(TRISCbits.TRISC4)
#define SPIRAM_SDO_TRIS		(TRISCbits.TRISC5)
#define SPIRAM_SPI_IF		(PIR1bits.SSPIF)
#define SPIRAM_SSPBUF		(SSP1BUF)
#define SPIRAM_SPICON1		(SSP1CON1)
#define SPIRAM_SPICON1bits	(SSP1CON1bits)
#define SPIRAM_SPICON2		(SSP1CON2)
#define SPIRAM_SPISTAT		(SSP1STAT)
#define SPIRAM_SPISTATbits	(SSP1STATbits)
#define SPIRAM2_CS_TRIS		(TRISEbits.TRISE5)
#define SPIRAM2_CS_IO		(LATEbits.LATE5)
#define SPIRAM2_SCK_TRIS	(TRISCbits.TRISC3)
#define SPIRAM2_SDI_TRIS	(TRISCbits.TRISC4)
#define SPIRAM2_SDO_TRIS	(TRISCbits.TRISC5)
#define SPIRAM2_SPI_IF		(PIR1bits.SSPIF)
#define SPIRAM2_SSPBUF		(SSP1BUF)
#define SPIRAM2_SPICON1		(SSP1CON1)
#define SPIRAM2_SPICON1bits	(SSP1CON1bits)
#define SPIRAM2_SPICON2		(SSP1CON2)
#define SPIRAM2_SPISTAT		(SSP1STAT)
#define SPIRAM2_SPISTATbits	(SSP1STATbits)

// VLSI VS1011/VS1053 audio encoder/decoder and DAC
#define MP3_DREQ_TRIS		(TRISBbits.TRISB0)	// Data Request
#define MP3_DREQ_IO 		(PORTBbits.RB0)
#define MP3_XRESET_TRIS		(TRISDbits.TRISD0)	// Reset, active low
#define MP3_XRESET_IO		(LATDbits.LATD0)
#define MP3_XDCS_TRIS		(TRISBbits.TRISB1)	// Data Chip Select
#define MP3_XDCS_IO			(LATBbits.LATB1)
#define MP3_XCS_TRIS		(TRISBbits.TRISB2)	// Control Chip Select
#define MP3_XCS_IO			(LATBbits.LATB2)
#define MP3_SCK_TRIS		(TRISCbits.TRISC3)
#define MP3_SDI_TRIS		(TRISCbits.TRISC4)
#define MP3_SDO_TRIS		(TRISCbits.TRISC5)
#define MP3_SPI_IF			(PIR1bits.SSP1IF)
#define MP3_SSPBUF			(SSP1BUF)
#define MP3_SPICON1			(SSP1CON1)
#define MP3_SPICON1bits		(SSP1CON1bits)
#define MP3_SPICON2			(SSP1CON2)
#define MP3_SPISTAT			(SSP1STAT)
#define MP3_SPISTATbits		(SSP1STATbits)

// OLED Display


// UART mapping functions for consistent API names across 8-bit and 16 or 
// 32 bit compilers.  For simplicity, everything will use "UART" instead 
// of USART/EUSART/etc.
#define BusyUART()			BusyUSART()
#define CloseUART()			CloseUSART()
#define ConfigIntUART(a)	ConfigIntUSART(a)
#define DataRdyUART()		DataRdyUSART()
#define OpenUART(a,b,c)		OpenUSART(a,b,c)
#define ReadUART()			ReadUSART()
#define WriteUART(a)		WriteUSART(a)
#define getsUART(a,b,c)		getsUSART(b,a)
#define putsUART(a)			putsUSART(a)
#define getcUART()			ReadUSART()
#define putcUART(a)			WriteUSART(a)
#define putrsUART(a)		putrsUSART((far rom char*)a)

#endif // #ifndef HARDWARE_PROFILE_H
