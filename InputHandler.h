/* 
 * File:   InputHandler.h
 * Author: Agust�n Gonzalez
 *
 * Created on 1 de noviembre de 2018, 18:03
 */

#ifndef INPUTHANDLER_H
#define	INPUTHANDLER_H
#include "TCPIP.h"
#include "TCP.h"

/******************** Public Prototype Functions ******************************/
void LeerEstadoEntradas(void);
char LeerEntradas(void);
void EnviarInputs(TCP_SOCKET MySocket,  char estado_previo_entradas);
/******************************************************************************/


#endif	/* INPUTHANDLER_H */


