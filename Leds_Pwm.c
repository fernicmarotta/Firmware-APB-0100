/* 
 * File:   Leds_Pwm.h
 * Author: Fernando Marotta
 *
 * Created on 6 de octubre de 2018, 21:43
 */
#include "Config_Version_and_ID.h"
#include "p18f97j60.h"
#include "Leds_Pwm.h"

/********************* Definitions ********************************************/
#define REGISTER_VALUE_DUTY_PERCENT 300 //! Valor a modificar para el duty cycle



enum {
ROJO = 1,
VERDE,
AMARILLO,
AZUL,
BLANCO
};

/******************** Public Prototype Functions ******************************/
/*
void Leds_Pwm_Init_PWM();
void Leds_Pwm_Set_Duty_PWM(unsigned int CUAL, unsigned long DUTY);
*/
/******************** Private Prototype Functions *****************************/
void Set_Duty_RC2( unsigned long DUTY );
void Set_Duty_RC1( unsigned long DUTY );
void Set_Duty_RG3( unsigned long DUTY );
void Set_Duty_RG4( unsigned long DUTY );
void Set_Duty_R( unsigned long DUTY );

/******************** Globals Variables ***************************************/ 
 
/******************** Private Funcionts ***************************************/
void Set_Duty_RC2( unsigned long DUTY ){
      if(DUTY & 0b0000000000000001){
        CCP1CONbits.DC1B0= 1;
        }
      else{
        CCP1CONbits.DC1B0= 0;
        }
      if(DUTY & 0b0000000000000010){
        CCP1CONbits.DC1B1= 1;
        }
      else{
        CCP1CONbits.DC1B1= 0;
        }
      CCPR1L = (int)DUTY >>2; //Copiar los 8 bits MSB de DUTY en CCPR1L;
}    

void Set_Duty_RC1 ( unsigned long DUTY ){ 
    if(DUTY & 0b0000000000000001){
        CCP2CONbits.DC2B0= 1;
        }
    else{
        CCP2CONbits.DC2B0= 0;
        }
      if(DUTY & 0b0000000000000010){
        CCP2CONbits.DC2B1= 1;
        }
      else{
        CCP2CONbits.DC2B1= 0;
        }
      CCPR2L = (int)DUTY >>2; //Copiar los 8 bits MSB de DUTY en CCPR1L;
}

void Set_Duty_RG3 ( unsigned long DUTY ){
    if(DUTY & 0b0000000000000001){
        CCP4CONbits.DC4B0= 1;
    }
    else{
        CCP4CONbits.DC4B0= 0;
    }
    if(DUTY & 0b0000000000000010){
        CCP4CONbits.DC4B1= 1;
    }
    else{
        CCP4CONbits.DC4B1= 0;
    }
    CCPR4L = (int)DUTY >>2; //Copiar los 8 bits MSB de DUTY en CCPR1L;    
}

void Set_Duty_RG4 ( unsigned long DUTY ){
    if(DUTY & 0b0000000000000001){
        CCP5CONbits.DC5B0= 1;
    }
    else{
        CCP5CONbits.DC5B0= 0;
    }
    if(DUTY & 0b0000000000000010){
        CCP5CONbits.DC5B1= 1;
    }
    else{
        CCP5CONbits.DC5B1= 0;
    }
    CCPR5L = (int)DUTY >>2; //Copiar los 8 bits MSB de DUTY en CCPR1L;    
}

void Set_Duty_RG0 ( unsigned long DUTY ){
    if(DUTY & 0b0000000000000001){
        CCP3CONbits.DC3B0= 1;
    }
    else{
        CCP3CONbits.DC3B0= 0;
    }
    if(DUTY & 0b0000000000000010){
        CCP3CONbits.DC3B1= 1;
    }
    else{
        CCP3CONbits.DC3B1= 0;
    }
    CCPR3L = (int)DUTY >>2; //Copiar los 8 bits MSB de DUTY en CCPR1L;
    
}

/******************** Public Functions ****************************************/

void Leds_Pwm_Init_PWM( void ){
      //setup timer 2
    T2CON = 0x78|0x05;
    PR2 = 0x4E;
    
    /* PWM */
    
    /* SETUP PWM PC2 */ 
    TRISCbits.TRISC2=0;LATCbits.LATC2=0;CCP1CON = 0x0c;ECCP1DEL = 0;ECCP1AS =0;
    CCP1CONbits.P1M1=0;CCP1CONbits.P1M0=0;CCP1CONbits.CCP1M3=0;
    CCP1CONbits.CCP1M2=0;CCP1CONbits.CCP1M1=0;CCP1CONbits.CCP1M0=0;

    /* SETUP PWM PC1 */ 
    TRISCbits.TRISC1=0;LATCbits.LATC1=0;CCP2CON = 0x0C;ECCP2DEL = 0;ECCP2AS = 0;
    CCP2CONbits.P2M1=0;CCP2CONbits.P2M0=0;CCP2CONbits.CCP2M3=0;
    CCP2CONbits.CCP2M2=0;CCP2CONbits.CCP2M1=0;CCP2CONbits.CCP2M0=0;

    /* SETUP PWM PG0 */
    TRISGbits.TRISG0= 0;LATGbits.LATG0=0;CCP3CON= 0x0C;ECCP3DEL = 0;ECCP3AS = 0;
    CCP3CONbits.P3M1=0;CCP3CONbits.P3M0=0;CCP3CONbits.CCP3M3=0;
    CCP3CONbits.CCP3M2=0;CCP3CONbits.CCP3M1=0;CCP3CONbits.CCP3M0=0;

    /* SETUP PWM PG3 */
    TRISGbits.TRISG3=0;LATGbits.LATG3=0;CCP4CON=0X0C;
    CCP4CONbits.CCP4M3=0;CCP4CONbits.CCP4M2=0;
    CCP4CONbits.CCP4M1=0;CCP4CONbits.CCP4M0=0;

    /* SETUP PWM PG4 */
    TRISGbits.TRISG4=0;LATGbits.LATG4=0;CCP5CON=0X0C;
    CCP5CONbits.CCP5M3=0;CCP5CONbits.CCP5M2=0;
    CCP5CONbits.CCP5M1=0;CCP5CONbits.CCP5M0=0;
}


void Leds_Pwm_Set_Duty_PWM(unsigned int CUAL, unsigned long DUTY){
DUTY = (DUTY * REGISTER_VALUE_DUTY_PERCENT )/100;
switch (CUAL){   
    case ROJO: //RC2
        Set_Duty_RC2( DUTY );
        Leds_Pwm_On( ROJO );
    break;
    
    case VERDE://RC1
        Set_Duty_RC1( DUTY );
        Leds_Pwm_On( VERDE );
    break;

    case AMARILLO://4://RG3
        Set_Duty_RG3( DUTY );
        Leds_Pwm_On( AMARILLO );
    break;

    case AZUL://5://RG4
        Set_Duty_RG4( DUTY );
        Leds_Pwm_On( AZUL );
    break;

    case BLANCO://3://RG0
        Set_Duty_RG0( DUTY );
        Leds_Pwm_On( BLANCO );        
    break;
    
    default:
    break;
    }
}

void Leds_Pwm_On(unsigned int CUAL){
 
 switch (CUAL){   
    case ROJO: //RC2
        CCP1CONbits.P1M1=0;CCP1CONbits.P1M0=0;CCP1CONbits.CCP1M3=1;
        CCP1CONbits.CCP1M2=1;CCP1CONbits.CCP1M1=0;CCP1CONbits.CCP1M0=0;
    break;
    
    case VERDE://RC1
        CCP2CONbits.P2M1=0;CCP2CONbits.P2M0=0;CCP2CONbits.CCP2M3=1;
        CCP2CONbits.CCP2M2=1;CCP2CONbits.CCP2M1=0;CCP2CONbits.CCP2M0=0;
    break;

    case AMARILLO://4://RG3
        CCP4CONbits.CCP4M3=1;CCP4CONbits.CCP4M2=1;
        CCP4CONbits.CCP4M1=0;CCP4CONbits.CCP4M0=0;
    break;

    case AZUL://5://RG4
        CCP5CONbits.CCP5M3=1;CCP5CONbits.CCP5M2=1;
        CCP5CONbits.CCP5M1=0;CCP5CONbits.CCP5M0=0;
    break;

    case BLANCO://3://RG0
        CCP3CONbits.P3M1=0;CCP3CONbits.P3M0=0;
        CCP3CONbits.CCP3M3=1;CCP3CONbits.CCP3M2=1;
        CCP3CONbits.CCP3M1=0;CCP3CONbits.CCP3M0=0;
    break;
    
    default:
    break;
    }   
}


void Leds_Pwm_Off(unsigned int CUAL){
 
 switch (CUAL){   
    case ROJO: //RC2
        CCP1CONbits.P1M1=0;CCP1CONbits.P1M0=0;CCP1CONbits.CCP1M3=0;
        CCP1CONbits.CCP1M2=0;CCP1CONbits.CCP1M1=0;CCP1CONbits.CCP1M0=0;
    break;
    
    case VERDE://RC1
        CCP2CONbits.P2M1=0;CCP2CONbits.P2M0=0;CCP2CONbits.CCP2M3=0;
        CCP2CONbits.CCP2M2=0;CCP2CONbits.CCP2M1=0;CCP2CONbits.CCP2M0=0;
    break;

    case AMARILLO://4://RG3
        CCP4CONbits.CCP4M3=0;CCP4CONbits.CCP4M2=0;
        CCP4CONbits.CCP4M1=0;CCP4CONbits.CCP4M0=0;
    break;

    case AZUL://5://RG4
        CCP5CONbits.CCP5M3=0;CCP5CONbits.CCP5M2=0;
        CCP5CONbits.CCP5M1=0;CCP5CONbits.CCP5M0=0;
    break;

    case BLANCO://3://RG0
        CCP3CONbits.P3M1=0;CCP3CONbits.P3M0=0;CCP3CONbits.CCP3M3=0;
        CCP3CONbits.CCP3M2=0;CCP3CONbits.CCP3M1=0;CCP3CONbits.CCP3M0=0;
    break;
    
    default:
    break;
    }   
}

/******************** Api Initialization **************************************/
/*
API_LEDS_PWM Leds_Pwm = {
Init_PWM = Leds_Pwm_Init_PWM,
Set_Duty_PWM = Leds_Pwm_Set_Duty_PWM,
};
*/

