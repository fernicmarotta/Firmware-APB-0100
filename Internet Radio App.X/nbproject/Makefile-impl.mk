#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a pre- and a post- target defined where you can add customization code.
#
# This makefile implements macros and targets common to all configurations.
#
# NOCDDL


# Building and Cleaning subprojects are done by default, but can be controlled with the SUB
# macro. If SUB=no, subprojects will not be built or cleaned. The following macro
# statements set BUILD_SUB-CONF and CLEAN_SUB-CONF to .build-reqprojects-conf
# and .clean-reqprojects-conf unless SUB has the value 'no'
SUB_no=NO
SUBPROJECTS=${SUB_${SUB}}
BUILD_SUBPROJECTS_=.build-subprojects
BUILD_SUBPROJECTS_NO=
BUILD_SUBPROJECTS=${BUILD_SUBPROJECTS_${SUBPROJECTS}}
CLEAN_SUBPROJECTS_=.clean-subprojects
CLEAN_SUBPROJECTS_NO=
CLEAN_SUBPROJECTS=${CLEAN_SUBPROJECTS_${SUBPROJECTS}}


# Project Name
PROJECTNAME=Internet Radio App.X

# Active Configuration
DEFAULTCONF=SEMAFORO_4
CONF=${DEFAULTCONF}

# All Configurations
ALLCONFS=default SEMAFORO_1 SEMAFORO_2 SEMAFORO_3 SEMAFORO_4 SEMAFORO_5 SEMAFORO_6 SEMAFORO_7 SEMAFORO_8 SEMAFORO_9 SEMAFORO_10 SEMAFORO_11 SEMAFORO_12 SEMAFORO_13 


# build
.build-impl: .build-pre
	${MAKE} -f nbproject/Makefile-${CONF}.mk SUBPROJECTS=${SUBPROJECTS} .build-conf


# clean
.clean-impl: .clean-pre
	${MAKE} -f nbproject/Makefile-${CONF}.mk SUBPROJECTS=${SUBPROJECTS} .clean-conf

# clobber
.clobber-impl: .clobber-pre .depcheck-impl
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=default clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_1 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_2 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_3 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_4 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_5 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_6 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_7 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_8 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_9 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_10 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_11 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_12 clean
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_13 clean



# all
.all-impl: .all-pre .depcheck-impl
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=default build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_1 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_2 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_3 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_4 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_5 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_6 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_7 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_8 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_9 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_10 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_11 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_12 build
	    ${MAKE} SUBPROJECTS=${SUBPROJECTS} CONF=SEMAFORO_13 build



# dependency checking support
.depcheck-impl:
#	@echo "# This code depends on make tool being used" >.dep.inc
#	@if [ -n "${MAKE_VERSION}" ]; then \
#	    echo "DEPFILES=\$$(wildcard \$$(addsuffix .d, \$${OBJECTFILES}))" >>.dep.inc; \
#	    echo "ifneq (\$${DEPFILES},)" >>.dep.inc; \
#	    echo "include \$${DEPFILES}" >>.dep.inc; \
#	    echo "endif" >>.dep.inc; \
#	else \
#	    echo ".KEEP_STATE:" >>.dep.inc; \
#	    echo ".KEEP_STATE_FILE:.make.state.\$${CONF}" >>.dep.inc; \
#	fi
