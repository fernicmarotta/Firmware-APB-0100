/* 
 * File:   IOConfiguration.c
 * Author: Agustin Gonzalez / Fernando Marotta
 *
 * Created on 1 de Noviembre de 2018, 18:00
 */

#include "Config_Version_and_ID.h"
#include "GenericTypeDefs.h"
#include "IOConfiguration.h"
#include "HardwareProfile.h"
#include "InputHandler.h"
#include <string.h>

/******************** Definitions *********************************************/
#define INPUT_1 0x01
#define INPUT_2 0x02
#define INPUT_3 0x04
#define INPUT_4 0x08
#define INPUT_5 0x10
/******************** Public Prototype Functions ******************************/
/*
 void LeerEstadoEntradas(void);
*/

/******************** Private Prototype Functions *****************************/
void ValorInput1(void);
void ValorInput2(void);
void ValorInput3(void);
void ValorInput4(void);
void ValorInput5(void);
/******************** Extern Variables ****************************************/

/******************** Globals Variables ***************************************/ 
#ifdef HABILITAR_ENTRADAS
int Rebote_input1_true=1;
int Rebote_input1_false=1;
int Rebote_input2_true=1;
int Rebote_input2_false=1;
int Rebote_input3_true=1;
int Rebote_input3_false=1;
int Rebote_input4_true=1;
int Rebote_input4_false=1;
int Rebote_input5_true=1;
int Rebote_input5_false=1;
BOOL input1_aux= 0;
BOOL input2_aux= 0;
BOOL input3_aux= 0;
BOOL input4_aux= 0;
BOOL input5_aux= 0;
const int antirebote=10;
#endif
char estado_entradas=0;

/******************** Private Funcionts ***************************************/



/******************** Public Functions ****************************************/
//void LeerEstadoEntradas(void);

/*********************************************************************
 * Function:        void LeerEstadoEntradas(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        Lee el pin RH0 para detectar si es de d�a o de noche 
 *                  y se lo env�a al servidor.
 *
 * Note:            None
 ********************************************************************/
void LeerEstadoEntradas(void){
#ifdef HABILITAR_ENTRADAS
    #ifdef INPUT1_I
    ValorInput1();
    #endif
    #ifdef INPUT2_I
    ValorInput2();
    #endif
    #ifdef INPUT3_I
    ValorInput3();
    #endif
    #ifdef INPUT4_I
    ValorInput4();
    #endif
    #ifdef INPUT5_I
    ValorInput5();
    #endif
#endif
}

/*********************************************************************
 * Function:        void EnviarInputs(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        Esta funci�n sirve para pasar el estado de las entradas
 *                  sin compartir la variable globalmente
 *                  
 *
 * Note:            None
 ********************************************************************/
char LeerEntradas( ){

return estado_entradas;
}


/*********************************************************************
 * Function:        void ValorInput1(void)
 *                  void ValorInput2(void)
 *                  void ValorInput3(void)
 *                  void ValorInput4(void)
 *                  void ValorInput5(void)
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        Antirebote individual para cada entrada                  
 *
 * Note:            None
 ********************************************************************/
void ValorInput1(){
#ifdef INPUT1_I
if(INPUT1_I == TRUE && Rebote_input1_false==0 && Rebote_input1_true==0)
    {
    Rebote_input1_true = antirebote;
    if(input1_aux == FALSE){
      input1_aux = TRUE; 
      estado_entradas |= INPUT_1;//SETEAR BIT
      }
    }
if(Rebote_input1_true>0)Rebote_input1_true--;

if(INPUT1_I == FALSE && Rebote_input1_false==0 && Rebote_input1_true==0)
    {
    Rebote_input1_false = antirebote;
    if(input1_aux == TRUE){
      input1_aux = FALSE; 
      estado_entradas &= ~INPUT_1;//LIMPIAR BIT
      }
    }
if(Rebote_input1_false>0)Rebote_input1_false--;
#endif
}

void ValorInput2(){
#ifdef INPUT2_I
if(INPUT2_I == TRUE && Rebote_input2_false==0 && Rebote_input2_true==0)
    {
    Rebote_input2_true = antirebote;
    if(input2_aux == FALSE){
      input2_aux = TRUE; 
      estado_entradas |= INPUT_2;//SETEAR BIT
      }
    }
if(Rebote_input2_true>0)Rebote_input2_true--;

if(INPUT2_I == FALSE && Rebote_input2_false==0 && Rebote_input2_true==0)
    {
    Rebote_input2_false = antirebote;
    if(input2_aux == TRUE){
      input2_aux = FALSE; 
      estado_entradas &= ~INPUT_2;//LIMPIAR BIT
      }
    }
if(Rebote_input2_false>0)Rebote_input2_false--;
#endif
}

void ValorInput3(){
#ifdef INPUT3_I
if(INPUT3_I == TRUE && Rebote_input3_false==0 && Rebote_input3_true==0)
    {
    Rebote_input3_true = antirebote;
    if(input3_aux == FALSE){
      input3_aux = TRUE; 
      estado_entradas |= INPUT_3;//SETEAR BIT
      }
    }
if(Rebote_input3_true>0)Rebote_input3_true--;

if(INPUT3_I == FALSE && Rebote_input3_false==0 && Rebote_input3_true==0)
    {
    Rebote_input3_false = antirebote;
    if(input3_aux == TRUE){
      input3_aux = FALSE; 
      estado_entradas &= ~INPUT_3;//LIMPIAR BIT
      }
    }
if(Rebote_input3_false>0)Rebote_input3_false--;
#endif
}

void ValorInput4(){
#ifdef INPUT4_I
if(INPUT4_I == TRUE && Rebote_input4_false==0 && Rebote_input4_true==0)
    {
    Rebote_input4_true = antirebote;
    if(input4_aux == FALSE){
      input4_aux = TRUE; 
      estado_entradas |= INPUT_4;//SETEAR BIT
      }
    }
if(Rebote_input4_true>0)Rebote_input4_true--;

if(INPUT4_I == FALSE && Rebote_input4_false==0 && Rebote_input4_true==0)
    {
    Rebote_input4_false = antirebote;
    if(input4_aux == TRUE){
      input4_aux = FALSE; 
      estado_entradas &= ~INPUT_4;//LIMPIAR BIT
      }
    }
if(Rebote_input4_false>0)Rebote_input4_false--;
#endif
}

void ValorInput5(){
#ifdef INPUT5_I
if(INPUT5_I == TRUE && Rebote_input5_false==0 && Rebote_input5_true==0)
    {
    Rebote_input5_true = antirebote;
    if(input5_aux == FALSE){
      input5_aux = TRUE; 
      estado_entradas |= INPUT_5;//SETEAR BIT
      }
    }
if(Rebote_input5_true>0)Rebote_input5_true--;

if(INPUT5_I == FALSE && Rebote_input5_false==0 && Rebote_input5_true==0)
    {
    Rebote_input5_false = antirebote;
    if(input5_aux == TRUE){
      input5_aux = FALSE; 
      estado_entradas &= ~INPUT_5;//LIMPIAR BIT
      }
    }
if(Rebote_input5_false>0)Rebote_input5_false--;
#endif
}


/*********************************************************************
 * Function:        void EnviarInputs(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        TRAMA ACTIVACI�N PULSADORES INSTANTANEOS API:
 *                  1: SEMAFORO ==> SERVIDOR >INPUTB1B2<
 *                  2: SERVIDOR ==> SEMAFORO >INPUTOK<
 *                                  B1: NUMERO DE ENTRADA 1 a 255 
 *                                  B2: VALOR DE ENTRADA 0 - 1
 *
 * Note:            None
 ********************************************************************/
void EnviarInputs(TCP_SOCKET MySocket,  char estado_previo_entradas){
    
    char inputdianoche[10]={0};
    int i = 0;
    
    for( i = 0; i < 5; i++ ){
        if( ((estado_previo_entradas >> i)& 0x01)  != ((estado_entradas >> i)& 0x01) ){
            sprintf(inputdianoche,">INPUT%c%c<", i+1, ((estado_entradas >> i)& 0x01)); 
            TCPPutArray(MySocket, inputdianoche, sizeof inputdianoche - 1);
            TCPFlush(MySocket);
            //Necesita delay???
            }
    }
   
}