/* 
 * File:   Command_Parser.h
 * Author: Fernando Marotta
 *
 * Created on 6 de octubre de 2018, 21:43
 */
#include "Config_Version_and_ID.h"
#include "Leds_Pwm.h"
#include <TCPIP.h>
#include <string.h>
#include "flash.h"
#include "Command_Parser.h"
#include "Command_Execution.h"



/********************* Definitions ********************************************/



/******************** Public Prototype Functions ******************************/
/*
void Command_Parser_Parser_Command(TCP_SOCKET MySocket2, char * TramaValida, int indice);
void Command_Parser_Restore_Saved_State( void );
*/
/******************** Private Prototype Functions *****************************/


/******************** Extern Variables ****************************************/
extern WORD  conectado;
#ifdef CHICHARRA
extern DWORD timer_chicharra;
extern BOOL  chicharra_flag;
#endif
extern BOOL  playf;
extern BOOL  play_streaming;


/******************** Globals Variables ***************************************/ 
unsigned int PWM_ON_OFF_P_DN_R[11]={0,0,0,0,0,0,0,0,0,0,0};
unsigned int PWM_ON_OFF_P_DN_R_TEMP[11];
unsigned int PWM_DUTY_R[5]={40,40,40,40,40};//0 a 100
unsigned int PWM_DUTY_D_MIN[5]={100,100,100,100,100};//0 a 100
unsigned int PWM_DUTY_N_MAX[5]={60,60,60,60,60};// 0 a 100
#pragma romdata reserved = 0xFF80
rom const unsigned char reserved[64]={0};
#pragma romdata
/******************** Private Funcionts ***************************************/


/******************** Public Functions ****************************************/

void Command_Parser_Restore_Saved_State( void ){
     
   char MEM_TEMP[64]={0};
   char buff_temp[30];
   unsigned int i=0;
   TCP_SOCKET My_Socket = INVALID_SOCKET;
     
   ReadFlash(65408,64,MEM_TEMP);  
   
   #warning "Aca se leia el estado previo, de las luces"
   //for(i=0;i<=10;i++){if(MEM_TEMP[i]<21){PWM_ON_OFF_P_DN_R[i]=MEM_TEMP[i];}}
   #warning "Aca se lee los paramatres de brillo configurado"
   for(i=11;i<=15;i++){if(MEM_TEMP[i] <101){PWM_DUTY_D_MIN[i-11]=MEM_TEMP[i];}}
   
   for(i=16;i<=20;i++){if(MEM_TEMP[i] <101){PWM_DUTY_N_MAX[i-16]=MEM_TEMP[i];}}
      
   for(i=21;i<=25;i++){ if(MEM_TEMP[i] <101){PWM_DUTY_R[i-21]=MEM_TEMP[i];}}
   
   /* Envio de ultima trama de activacion antes de que se apagara el equipo */
   buff_temp[0]='>';buff_temp[1]='A';buff_temp[2]='L';buff_temp[3]='S';
   for(i=0;i<11;i++){buff_temp[i+4]=PWM_ON_OFF_P_DN_R[i];}
   buff_temp[15]='<';buff_temp[16]=0;
 
   Command_Parser_Parser_Command(My_Socket,buff_temp,16);//>ALS <
}



void Command_Parser_Parser_Command(TCP_SOCKET MySocket2, char * TramaValida, int indice)
{

int ptr=0;
int i=0,respuesta=100;

/*Lista de comandos*/
enum{
ALS = 0,
PWM,//CONFIGURACIONO SALIDAS PWM
PRESENTACION,//PRESENTACION
SOK,
VOL,
API,
TOTAL_COMMANDS
};
char CSEMAFORO[TOTAL_COMMANDS][8]=
{
 "ALS\0",//ACTIVACION LUMINICA
 "PWM\0",//CONFIGURACIONO SALIDAS PWM
 ">$?<\0",//PRESENTACION
 ">SOK<\0",
 "VOL",
 "API"
};

  for(i=0; i<TOTAL_COMMANDS ;i++)
      { 
      if(strstr(TramaValida,CSEMAFORO[i]) != NULL)
         {
         respuesta=i;
         i=TOTAL_COMMANDS;
         }
      }


 switch(respuesta)
       {
        case ALS://>ALSP1D1M1B1B2B3B4B5B6B7B8<//ACTIVACION LUMINICA SEMAFORO
        /*1: SERVIDOR ==> SEMAFORO >ALSP1D1M1B1B2B3B4B5B6B7B8<//ACTIVACION LUMINICA SEMAFORO
            P1: PRIORIDAD //MAYOR NUMERO (255)--> MAYOR PRIORIDAD = MAS TIEMPO EN SEMAFORO(VER TIMER DISPONIBLE)
            D1: DOTACION DE 1 A 9 // A 7 SEGMENTOS_2
            M1: MOVIL 1 AL 20 // A 7 SEGMENTOS_1
            B1: ROJO     = 1 � 0
            B2: VERDE    = 1 � 0
            B3: AMARILLO = 1 � 0
            B4: AZUL     = 1 � 0
            B5: BLANCO   = 1 � 0
            B6: PERSONAS ATRAPADAS 1 � 0
            B7: DIA/NOCHE 1 � 0 //PONE INTENSIDAD AL MINIMO(70%DEL MAXIMO)/MAXIMO RESPECTIVAMENTE(MAXIMO CONFIGURADO EN TRAMA PWM)
                      //(TODOS LOS COLORES)
            B8: REPOSO //PONE INTENSIDAD AL 40%(TODOS LOS COLORES)
        2: SEMAFORO ==> SERVIDOR >ALSOK<  */
            Command_Execution_ALS(MySocket2,TramaValida,indice , TRUE);
        
        break;

        case PWM://>PWMC1R1D1L1H1<
        /*TRAMA CONFIGURACION PWM:
        1: SERVIDOR ==> SEMAFORO >PWMC1R1D1L1H1<
				C1: COLOR: 1= ROJO 2=VERDE 3=AMARILLO 4=AZUL 5=BLANCO
				D1: DUTY_CYCLE 0 A 100% DE REPOSO
				R1: 0= MAX_MIN � 1=REPOSO
				L1: LOW= PORCENTAJE MINIMO //DIA
				H1: HIHG= PORCENTAJE MAXIMO//NOCHE
        2: SEMAFORO ==> SERVIDOR >PWMOK<
        */
            Command_Execution_PWM(MySocket2,TramaValida,indice);    
        break;


        case PRESENTACION:
            Command_Execution_PRESENTACION(MySocket2);      
        break;

        case SOK:
            conectado--;
        break;
        
        case VOL://>VOLB1<
            Command_Execution_VOL(MySocket2,TramaValida,indice);
        break;
        
        #ifdef TRAMA_API
        case API://>APIB1B2<
        /*TRAMA ACTIVACI�N PULSADORES INSTANTANEOS API:
        1: SERVIDOR ==> SEMAFORO >APIB1B2<
				B1: SEMAFORO ESQUINA COLON 0=Apagado 1=Prendido
				B2: SIRENA MAYOR 0=Apagada 1=Prendida
		2: SEMAFORO ==> SERVIDOR >APIOK<
        */
            Command_Execution_API(MySocket2,TramaValida,indice);
        break;
        #endif

        default:
            Command_Execution_NOT_FOUND(MySocket2);        
        break;
       }
memset(TramaValida,NULL, MAX_LENGTH_COMMAND );
}

/******************* API Initialization ***************************************/
/*
API_COMMAND_PARSER Command_Parser = {
Parser_Command = Command_Parser_Parser_Command,
Restore_Saved_State = Command_Parser_Restore_Saved_State,
};
*/


