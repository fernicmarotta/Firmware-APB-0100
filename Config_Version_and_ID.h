/* 
 * File:   Config_Version_and_ID.h
 * Author: Fernando Marotta
 *
 * Created on 7 de octubre de 2018, 03:14
 */

/********************Globals Definition ***************************************/
#define VER1 1 // N�mero de Versi�n del Firmware
#define VER2 0
#define VER3 1

/******************** Pre Compiler Semaphore Selection ************************/
#if SEMAFORO_ID == 1
    #define ID 001 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 2
    #define ID 002 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 3
    #define ID 003 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 4
    #define ID 004 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 5
    #define ID 005 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 6
    #define ID 006 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 7
    #define ID 007 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 8
    #define ID 008 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 9
    #define ID 009 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 10
    #define ID 010 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 11
    #define ID 011 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 12
    #define ID 012 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#elif  SEMAFORO_ID == 13
    #define ID 013 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#endif

#ifndef SEMAFORO_ID
    #define ID 001 // ID del sem�foro. Cambiar tambi�n la MAC y la IP en TCPIPConfig.h
#endif






#ifndef CONFIG_H
#define	CONFIG_H

#endif	/* CONFIG_H */

